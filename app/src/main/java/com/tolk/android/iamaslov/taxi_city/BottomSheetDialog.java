package com.tolk.android.iamaslov.taxi_city;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


public class BottomSheetDialog extends BottomSheetDialogFragment {
    private static final String TAG1 = "BottomSheetDialog";
    private BottomSheetListener mListener;
    private Button mButton1;
    private EditText mEditText1,mEditText2;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_layout, container, false);

        mEditText1 = v.findViewById(R.id.from);
        mEditText2 = v.findViewById(R.id.to);

        mButton1 = v.findViewById(R.id.btn_add_note);
        mButton1.setText("Заказать (3.3 блр/км)");
        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((mEditText1.getText().toString()).equals("")) {
                    Toast.makeText(getActivity(), "Заполните поле отправления", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getActivity(), "Заказ выполнен,ожидайте", Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            }
        });

        RadioGroup radioGroup1 = v.findViewById(R.id.radioGroupOfPayment);
        radioGroup1.check(R.id.card_payment);

        RadioGroup radioGroup2 = v.findViewById(R.id.radioGroup_bottom_taxi);
        radioGroup2.check(R.id.rtaxi1);

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rtaxi2){
                    mButton1.setText("Заказать (5.4 блр/км)");
                }else if (checkedId == R.id.rtaxi3){
                    mButton1.setText("Заказать (4.6 блр/км)");
                }else {
                    mButton1.setText("Заказать (3.3 блр/км)");
                }
            }
        });


        return v;
    }


    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (BottomSheetListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement BottomSheetListener");
        }

    }
}

