package com.tolk.android.iamaslov.taxi_city;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class PaymentActivity extends AppCompatActivity {
    private Button mSaveButton;
    private EditText mName, mNumber, mSvv, mDate;
    private RelativeLayout mRelativeLayout;
    private RadioGroup mRadioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Методы оплаты");
        mName = findViewById(R.id.name_oncard);
        mNumber = findViewById(R.id.card_number);
        mSvv = findViewById(R.id.cvv_code);
        mDate = findViewById(R.id.card_data);
        mRelativeLayout = findViewById(R.id.edit_text_relative);

        mRadioGroup = findViewById(R.id.radioGroup);
        mRadioGroup.check(R.id.card_payment);

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.cash){
                   mRelativeLayout.setVisibility(View.GONE);
                }else {
                    mRelativeLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        mSaveButton = findViewById(R.id.save_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRadioGroup.getCheckedRadioButtonId() == R.id.cash) {
                    finish();
                } else {
                   String name =  mName.getText().toString();
                   String number =  mNumber.getText().toString();
                   String svv =  mSvv.getText().toString();
                   String date = mDate.getText().toString();
                   if ((name.equals("")) ||(number.equals("")) || (svv.equals(""))  ||(date.equals("")) ){
                       Toast.makeText(PaymentActivity.this, "Пожалуйста ,заполните все поля", Toast.LENGTH_SHORT).show();
                   }else {
                       finish();
                   }

                }

            }
        });

    }
}
