package com.tolk.android.iamaslov.taxi_city;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import java.util.Objects;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.drag.DragAndDropItem;
import ru.yandex.yandexmapkit.overlay.drag.DragAndDropOverlay;
import ru.yandex.yandexmapkit.utils.GeoPoint;


/**
 * MapLayers.java
 * <p/>
 * This file is a part of the Yandex Map Kit.
 * <p/>
 * Version for Android © 2012 YANDEX
 * <p/>
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://legal.yandex.ru/mapkit/
 */
public class MapMyLocationChangeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BottomSheetDialog.BottomSheetListener {
    /**
     * Called when the activity is first created.
     */
    MapController mMapController;
    private DrawerLayout drawer;
    private OverlayManager mOverlayManager;
    public final String number = "+375447641862";

    private static final int PERMISSIONS_CODE = 109;
    private static final int PERMISSIONS_CODE_CALL = 1;
    private Button mNextButton;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.yandex_navigation);

        final MapView mapView = (MapView) findViewById(R.id.map);
        mapView.showBuiltInScreenButtons(true);
        mapView.showJamsButton(false);
        mMapController = mapView.getMapController();
        // add listener
        checkPermission();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(null);





        mNextButton = findViewById(R.id.next_order);

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog();
                bottomSheetDialog.show(getSupportFragmentManager(), "BottomSheetDialog");
            }
        });


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            navigationView.setCheckedItem(R.id.nav_home);
        }
        mOverlayManager = mMapController.getOverlayManager();
        mOverlayManager.getMyLocation().setEnabled(true);
        showObject();
    }

    public void showObject() {
        // Load the required resources
        Resources res = getResources();

        float density = getResources().getDisplayMetrics().density;
        int offsetX = (int) (-7 * density);
        int offsetY = (int) (20 * density);

        // Create a layer of objects for the map
        DragAndDropOverlay overlay = new DragAndDropOverlay(mMapController);

        // Create an object for the layer
        DragAndDropItem drag1Item = new DragAndDropItem(new GeoPoint(53.9, 27.57), res.getDrawable(R.mipmap.ic_pickup));
        // Set offsets of the image to match the balloon tail with the specified GeoPoint

        drag1Item.setOffsetX(offsetX);
        drag1Item.setOffsetY(offsetY);
        // Make the object draggable
        drag1Item.setDragable(true);

        // Create a balloon model for the object
        BalloonItem balloonDrar1 = new BalloonItem(this, drag1Item.getGeoPoint());
        balloonDrar1.setText(getString(R.string.drag));
        // Set the additional balloon offset
        balloonDrar1.setOffsetX(offsetX);
        // Add the balloon model to the object
        drag1Item.setBalloonItem(balloonDrar1);
        // Add the object to the layer
        overlay.addOverlayItem(drag1Item);


        // Add the layer to the map
        mOverlayManager.addOverlay(overlay);
    }

    private void checkPermission() {
        int permACL = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int permAFL = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permACL != PackageManager.PERMISSION_GRANTED ||
                permAFL != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_CODE);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMapController.getOverlayManager().getMyLocation().refreshPermission();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                }  // permission denied, boo! Disable the
                // functionality that depends on this permission.

                return;
            }

            case PERMISSIONS_CODE_CALL: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    makePhoneCall();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Intent intent;
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog();
                bottomSheetDialog.show(getSupportFragmentManager(), "BottomSheetDialog");
                break;
            case R.id.nav_addr:
                Toast.makeText(this, "Адреса зависят от базы данных", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_history:
                intent = new Intent(MapMyLocationChangeActivity.this, HistorySingleActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_pay:
                intent = new Intent(MapMyLocationChangeActivity.this, PaymentActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "Скачай мобильное приложение такси Сити по этой ссылке и получи первую поездку бесплатно" + "https://play.google.com/store/apps/details?id=" + getPackageName());
                sharingIntent.setType("*/*");
                startActivity(Intent.createChooser(sharingIntent, "Написать  другу в :"));

                break;
            case R.id.nav_call:
                makePhoneCall();
                break;
            case R.id.settings_menu:
                intent = new Intent(MapMyLocationChangeActivity.this, CustomerSettingsActivity.class);
                startActivity(intent);
        }
        // Handle navigation view item clicks here.
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void makePhoneCall() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CALL_PHONE
                           }, PERMISSIONS_CODE_CALL);
        }else {
            String dial = "tel:" + number;
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
        }

    }

    @Override
    public void onButtonClicked(String text) {

    }
}